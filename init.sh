#/bin/bash

username="candidat"
userpassword="candidat"

adduser $username --gecos '' --disabled-password
echo "$username:$username" | chpasswd

export ID=$(curl -s http://169.254.169.254/metadata/v1/id)
export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
echo "$ID $PUBLIC_IPV4" > ~/debug'
